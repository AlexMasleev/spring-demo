import org.example.entities.IntegerImpl;
import org.example.services.IntegerImplService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.List;

public class TestSpring {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        IntegerImplService service = ctx.getBean(IntegerImplService.class);
        Assert.notNull(service, "Service is null!");

        IntegerImpl integer1 = ctx.getBean("integerImpl", IntegerImpl.class);
        IntegerImpl integer2 = ctx.getBean("integerImpl", IntegerImpl.class);
        IntegerImpl integer3 = ctx.getBean("integerImpl", IntegerImpl.class);
        IntegerImpl integer4 = ctx.getBean("integerImpl", IntegerImpl.class);

        service.save(integer1);
        service.save(integer4);
        service.save(integer3);

        Assert.isTrue(service.saveAndShowId(integer2) == 4, "Index is wrong!");
        Assert.isTrue(service.getAll().size() == 4, "Size of repo is wrong!");

        List<IntegerImpl> list = Arrays.asList(new IntegerImpl(13), new IntegerImpl(17),
                new IntegerImpl(15), new IntegerImpl(18));

        service.saveAll(list);
        Assert.isTrue(service.getAll().size() == 8, "Size of repo is wrong!");

        Assert.isTrue(service.max().equals(ctx.getBean("max")), "Find max wrong!");
        Assert.isTrue(service.min().equals(ctx.getBean("min")), "Find min wrong!");
        Assert.isTrue(service.sum().equals(ctx.getBean("sum")), "Find sum wrong!");
        Assert.isTrue(service.avg().equals(ctx.getBean("avg")), "Find avg wrong!");

        Assert.isTrue(service.max(Arrays.asList(5,6,7,8)).equals(ctx.getBean("maxIn")), "Find max in sequence 1,2,3,4 wrong");
        Assert.isTrue(service.min(Arrays.asList(1,2,3,4)).equals(ctx.getBean("minIn")), "Find min in sequence 5,6,7,8 wrong!");
        Assert.isTrue(service.sum(Arrays.asList(1,2,3,4)).equals(ctx.getBean("sumIn")), "Find sum in sequence 1,2,3,4 wrong!");
        Assert.isTrue(service.avg(Arrays.asList(1,2,3,4,5,6,7,8)).equals(ctx.getBean("avgIn")), "Find avg in sequence 1,2,3,4,5,6,7,8 wrong!");

        System.out.println("All tests are passed! Congratulation!");
        ctx.close();
    }
}
