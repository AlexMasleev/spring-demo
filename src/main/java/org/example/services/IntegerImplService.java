package org.example.services;

import org.springframework.stereotype.Service;
import org.example.entities.IntegerImpl;
import org.example.repositories.IntegerImplRepository;

import java.util.List;

@Service
public class IntegerImplService  {
    private final IntegerImplRepository repository;

    public IntegerImplService(IntegerImplRepository repository) {
        this.repository = repository;
    }

    public List<IntegerImpl> getAll() {
        return repository.findAll();
    }

    public void save(IntegerImpl integer) {
        repository.save(integer);
    }

    public Integer saveAndShowId(IntegerImpl integer) {
        repository.save(integer);
        return integer.getId();
    }

    public void saveAll(List<IntegerImpl> list) {
        repository.saveAll(list);
    }

    public Integer sum() {
        return repository.sum();
    }

    public Integer max() {
        return repository.max();
    }

    public Integer min() {
        return repository.min();
    }

    public Double avg() {
        return repository.avg().doubleValue();
    }

    public Integer sum(List<Integer> list) {
        return repository.getValuesById(list).stream().mapToInt(Integer::intValue).sum();
    }

    public Integer min(List<Integer> list) {
        return repository.getValuesById(list).stream().mapToInt(Integer::intValue).min().getAsInt();
    }

    public Integer max(List<Integer> list) {
        return repository.getValuesById(list).stream().mapToInt(Integer::intValue).max().getAsInt();
    }

    public Double avg(List<Integer> list) {
        return repository.getValuesById(list).stream().mapToInt(Integer::intValue).average().getAsDouble();
    }
}
