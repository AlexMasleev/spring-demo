package org.example.utils;

import org.example.services.IntegerImplService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
public class InitiateUtils implements CommandLineRunner {
    private final IntegerImplService integerImplService;

    public InitiateUtils(IntegerImplService integerImplService) {
        this.integerImplService = integerImplService;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Server run!");
    }
}
