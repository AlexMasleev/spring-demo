package org.example.repositories;

import org.example.entities.IntegerImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IntegerImplRepository extends JpaRepository<IntegerImpl, Integer> {

    @Query("select sum(i.value) from IntegerImpl i")
    Integer sum();
    @Query("select min(i.value) from IntegerImpl i")
    Integer min();
    @Query("select avg(i.value) from IntegerImpl i")
    Integer avg();
    @Query("select max(i.value) from IntegerImpl i")
    Integer max();

    @Query("select i.value from IntegerImpl i where i.id in (:list)")
    List<Integer> getValuesById(List<Integer> list);
}
