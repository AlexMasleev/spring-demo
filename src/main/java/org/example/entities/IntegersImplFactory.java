package org.example.entities;

import java.util.Random;

public class IntegersImplFactory {
    public IntegerImpl createNewIntegerImpl() {
        Random random = new Random();
        return new IntegerImpl(random.nextInt(50));
    }
}
