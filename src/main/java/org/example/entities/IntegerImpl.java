package org.example.entities;

import javax.persistence.*;

@Entity
public class IntegerImpl {
    @Id
    @GeneratedValue
    private Integer id;

    private Integer value;

    public IntegerImpl(Integer value) {
        this.value = value;
    }

    public IntegerImpl() {

    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "IntegerImpl{" +
                "id=" + id +
                ", value=" + value +
                '}';
    }
}
